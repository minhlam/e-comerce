from django.db import models
# from django.contrib.auth.base_user import AbstractBaseUser


class Category(models.Model):
    name = models.CharField(max_length=100, unique=True)
    isActive = models.BooleanField(default=True)

    def __str__(self):
        return self.name


class Color(models.Model):
    name = models.CharField(max_length=100)
    isActive = models.BooleanField(default=True)

    def __str__(self):
        return self.name


class Material(models.Model):
    name = models.CharField(max_length=100, unique=True)
    isActive = models.BooleanField(default=True)

    def __str__(self):
        return self.name


class Brand(models.Model):
    name = models.CharField(max_length=100, unique=True)
    isActive = models.BooleanField(default=True)

    def __str__(self):
        return self.name


class Images(models.Model):
    name = models.CharField(max_length=200)
    image = models.ImageField(upload_to='images/')

    def __str__(self):
        return self.name


class Product(models.Model):
    name = models.CharField(max_length=200, unique=True)
    price = models.FloatField(default=0)
    description = models.TextField(max_length=800)
    detail = models.TextField(max_length=800)
    dateCreate = models.DateField(auto_now=True)
    isActive = models.BooleanField(default=True)
    brand = models.ForeignKey(Brand, on_delete=models.CASCADE)
    color = models.ManyToManyField(Color)
    material = models.ManyToManyField(Material)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    images = models.ForeignKey(Images, on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    def colors(self):
        return '\n'.join([p.name for p in self.color.all()])


class User(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    age = models.IntegerField()
    phone = models.IntegerField()
    isActive = models.BooleanField(default=True)
    address = models.TextField(max_length=300)
    email = models.EmailField(max_length=100, unique=True)
    is_customer = models.BooleanField(default=False)
    date_joined = models.DateTimeField(auto_now=True)
    last_login = models.DateTimeField(null=True)


class Order(models.Model):
    order_id = models.AutoField(primary_key=True)
    dateCreate = models.DateTimeField(auto_now=True)
    customer = models.OneToOneField(User, on_delete=models.PROTECT)

    def __str__(self):
        return self.order_id


class OrderDetail(models.Model):
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.IntegerField(default=1)


