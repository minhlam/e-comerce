from django.contrib import admin
from .models import *


admin.site.register(Category)
admin.site.register(Brand)
admin.site.register(Color)
admin.site.register(Material)
admin.site.register(Images)
admin.site.register(Product)
admin.site.register(Order)
admin.site.register(OrderDetail)
admin.site.register(User)

# Register your models here.
